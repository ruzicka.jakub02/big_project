# Big Project

This is one of my homework for the Python course. It also consists various tests in order to prove functionality.


## Getting started
1. Get this repository by using: <br>
`git clone https://gitlab.com/ruzicka.jakub02/big_project.git` <br>
or with SSH: <br>
`git@gitlab.com:ruzicka.jakub02/big_project.git`

## Installation
I recommend using virtual environment.
Commands for setting up a virtual environment
```
$ python3 -m venv venv_dir_path
$ source venv_dir_path/bin/activate
(venv) $ pip install pytest
(venv) $ python -m pytest tests.py
(venv) $ python -m pytest -v tests.py
(venv) $ python -m pytest -vv tests.py
```

## How to use
1. Inside the virtual environment run the script: `python -m pytest tests.py`
2. The script will show the results of the tests in tests.py file.