def is_palindrome(data):
    return True if data == data[::-1] else False


# https://www.programiz.com/python-programming/examples/ascii-character
def lex_compare(a, b):
    i = 0
    while i < len(a) and i < len(b):
        if ord(a[i]) < ord(b[i]):
            return a
        elif ord(a[i]) > ord(b[i]):
            return b
        i += 1
    if len(a) < len(b):
        return a
    else:
        return b


"""
    Lexicographically compare `a` with `b` and return the smaller string.
    Implement the comparison yourself, do not use the `<` operator for comparing strings :)

    Example:
        lex_compare('a', 'b') == 'a'
        lex_compare('ahoj', 'buvol') == 'ahoj'
        lex_compare('ahoj', 'ahojky') == 'ahoj'
        lex_compare('dum', 'automobil') == 'automobil'
    """


def count_successive(string):
    if string == "":
        return []
    tuples = []
    count = 1
    previous_char = string[0]

    for char in string[1:]:
        if char == previous_char:
            count += 1
        else:
            tuples.append((previous_char, count))
            previous_char = char
            count = 1
    tuples.append((previous_char, count))
    return tuples


"""
    Go through the string and for each character, count how many times it appears in succession.
    Store the character and the count in a tuple and return a list of such tuples.

    Example:
          count_successive("aaabbcccc") == [("a", 3), ("b", 2), ("c", 4)]
          count_successive("aba") == [("a", 1), ("b", 1), ("a", 1)]
"""


def find_positions(items):
    if items is None:
        return {}
    words = {}
    for i, item in enumerate(items):
        if item not in words:
            words[item] = [i]
        else:
            words[item].append(i)
    return words


"""
    Go through the input list of items and collect indices of each individual item.
    Return a dictionary where the key will be an item and its value will be a list of indices
    where the item was found.

    Example:
        find_positions(["hello", 1, 1, 2, "hello", 2]) == {
            2: [3, 5],
            "hello": [0, 4],
            1: [1, 2]
        }
    """


def invert_dictionary(dictionary):
    reversed_dictionary = dict(map(reversed, dictionary.items()))
    return dict(map(reversed, dictionary.items())) if len(reversed_dictionary) == len(dictionary) else None


"""
    Invert the input dictionary. Turn keys into values and vice-versa.
    If more values would belong to the same key, return None.

    Example:
        invert_dictionary({1: 2, 3: 4}) == {2: 1, 4: 3}
        invert_dictionary({1: 2, 3: 2}) is None
"""